# pylint: disable=C0103, C0301
# pylint: disable=too-few-public-methods
"""
This module provides utility classes for interacting with the Kuali API.

The Kuali class provides the base URL and headers for API requests.
The KualiQueries class provides GraphQL queries for retrieving user data.
The KualiMutations class provides GraphQL mutations for creating users.

Classes:
- Kuali: Provides URL and headers for API requests.
- KualiQueries: Provides GraphQL queries for retrieving user data.
- KualiMutations: Provides GraphQL mutations for creating users.
"""

# Standard library imports
import os
from enum import Enum
from pydantic import BaseModel # pylint: disable=no-name-in-module

# Third-party imports
from dotenv import load_dotenv

load_dotenv()


class JmespathQueries:
    """
     A utility class that formats incoming data into a structure adventageous 
     for developers to easily implement/set variables from inside of Kuali 
     Build's integration dashboard.
    """
    @staticmethod
    def duke_members():
        """
        A method to construct and organize otherwise confusing data structures from LDAP.
        Below are the naming conventions from LDAP and what (I presume) most of them 
        allude to.

        1. cn: Refers to a person's common name.
        2. count: Not sure - but think it may have something to do with a count of a grouped id?
        3. displayname: Refers to a person's display name.
        4. dkuid: Refers to a unique identifier assigned to a person.
        5. dn: Refers to the person's distinguished name in the data management system.
        6. duacmailboxexists: Indicates whether a person has an active mailbox associated with their account.
        7. dudegree: Refers to a person's academic degree(s).
        8. dudempoid: Refers to the Duke Employee ID associated with a person's record.
        9. dudempoidhist: Refers to the Duke Employee ID history associated with a person's record.
        10. dudukeid: Refers to a unique Duke University ID assigned to a person.
        11. dudukeidhistory: Refers to the history of Duke University IDs associated with a person's record.
        12. duldapkey: Refers to the LDAP key associated with a person's record in the data management system.
        13. dumiddlename1: Refers to a person's middle name.
        14. dupsacadcareerc1: Refers to a person's first academic career.
        15. dupsacadcareerc2: Refers to a person's second academic career.
        16. dupsacadcareerc3: Refers to a person's third academic career.
        17. dupsacadcareerdescc1: Refers to the description of a person's first academic career.
        18. dupsacadcareerdescc2: Refers to the description of a person's second academic career.
        19. dupsacadcareerdescc3: Refers to the description of a person's third academic career.
        20. dupsacadprogc1: Refers to a person's first academic program.
        21. dupsacadprogc2: Refers to a person's second academic program.
        22. dupsacadprogc3: Refers to a person's third academic program.
        23. dupsexpgradtermc1: Refers to a person's expected graduation term for their first academic program.
        24. dupsexpgradtermc2: Refers to a person's expected graduation term for their second academic program.
        25. dupsexpgradtermc3: Refers to a person's expected graduation term for their third academic program.
        26. duresearcherid: Refers to a unique researcher ID assigned to a person
        27. dusapcompany: Refers to the company associated with a person's record in the SAP system.
        28. dusapcompanydesc: Refers to the description of the company associated with a person's record in the SAP system.
        29. dusaporgunit: Refers to the organizational unit associated with a person's record in the SAP system.
        30. dusponsor: Refers to the sponsor of a person's record in the data management system.
        31. duuserprivacyrequest: Indicates whether a person has requested privacy settings for their account.
        32. edupersonaffiliation: Refers to a person's affiliation with Duke University (e.g. student, faculty, staff, alumni).
        33. edupersonnickname: Refers to a person's nickname or preferred name.
        34. edupersonprimaryaffiliation: Refers to a person's primary affiliation with Duke University.
        35. edupersonprincipalname: Refers to a person's principal name in the system.
        36. facsimiletelephonenumber: Refers to a person's fax number.
        37. generationqualifier: Refers to a person's generation qualifier (e.g. Jr., Sr.).
        38. gidnumber: Refers to the group ID number associated with a person's uid.
        39. givenname: Refers to a person's first name.
        40. homedirectory: Refers to a person's home directory.
        41. labeleduri: Refers to a labeled URI associated with a person's record.
        42. loginshell: Refers to the person's login shell.
        43. mail: Refers to a person's email address.
        44. ntuserhomedir: Refers to a person's home directory in a Windows-based system.
        45. objectclass: Refers to the object class of a person's record in the data management system.
        46. output: top level item in the json query.
        47. pager: Refers to a pager number associated with a person.
        48. person: Refers to a person's record in the data management system.
        49. sn: Refers to a person's last name or surname.
        50. uid: Refers to a unique user ID assigned to a person in the system.
        51. uidnumber: Refers to the user ID number associated with a person's record.

        returns:
        Same format as seen above, if the value does not exist or is empty, the
        returned value for that key then becomes null.
        """
        query = """
        output[].person[].{
            cn: cn | [?@ != null] | [join(`; `, @)] | [0],
            edupersonnickname: edupersonnickname | [?@ != null] | [join(`; `, @)] | [0],
            title: title | [?@ != null] | [join(`; `, @)] | [0],
            ou: ou | [?@ != null] | [join(`; `, @)] | [0],
            dusaporgunit: dusaporgunit | [?@ != null] | [join(`; `, @)] | [0],
            uidnumber: uidnumber | [?@ != null] | [join(`; `, @)] | [0],
            gidnumber: gidnumber | [?@ != null] | [join(`; `, @)] | [0],
            postaladdress: postaladdress | [?@ != null] | [join(`; `, @)] | [0],
            postofficebox: postofficebox | [?@ != null] | [join(`; `, @)] | [0],
            displayname: displayname | [?@ != null] | [join(`; `, @)] | [0],
            givenname: givenname | [?@ != null] | [join(`; `, @)] | [0],
            sn: sn | [?@ != null] | [join(`; `, @)] | [0],
            duacmailboxexists: duacmailboxexists | [?@ != null] | [join(`; `, @)] | [0],
            edupersonprincipalname: edupersonprincipalname | [?@ != null] | [join(`; `, @)] | [0],
            edupersonprimaryaffiliation: edupersonprimaryaffiliation | [?@ != null] | [join(`; `, @)] | [0],
            telephonenumber: telephonenumber | [?@ != null] | [join(`; `, @)] | [0],
            mail: mail | [?@ != null] | [join(`; `, @)] | [0],
            dumiddlename1: dumiddlename1 | [?@ != null] | [join(`; `, @)] | [0],
            facsimiletelephonenumber: facsimiletelephonenumber | [?@ != null] | [join(`; `, @)] | [0],
            duldapkey: duldapkey | [?@ != null] | [join(`; `, @)] | [0],
            dudukeid: dudukeid | [?@ != null] | [join(`; `, @)] | [0],
            dudukeidhistory: dudukeidhistory | [?@ != null] | [join(`; `, @)] | [0],
            uid: uid | [?@ != null] | [join(`; ` , @)] | [0],
            dusapcompany: dusapcompany | [?@ != null] | [join(`; ` , @)] | [0],
            dusapcompanydesc: dusapcompanydesc | [?@ != null] | [join(`; ` , @)] | [0],
            edupersonaffiliation: edupersonaffiliation | [?@ != null] | [join(`; ` , @)] | [0],
            count: count | [?@ != null] | [join(`; ` , @)] | [0],
            dn: dn | [?@ != null] | [join(`; ` , @)] | [0],
            person: person | [?@ != null] | [join(`; ` , @)] | [0],
            pager: pager | [?@ != null] | [join(`; ` , @)] | [0],
            generationqualifier: generationqualifier | [?@ != null] | [join(`; ` , @)] | [0],
            duuserprivacyrequest: duuserprivacyrequest | [?@ != null] | [join(`; ` , @)] | [0],
            dusponsor: dusponsor | [?@ != null] | [join(`; ` , @)] | [0],
            duuserprivacyrequest: duuserprivacyrequest | [?@ != null] | [join(`; ` , @)] | [0],
            duresearcherid: duresearcherid | [?@ != null] | [join(`; ` , @)] | [0],
            dkuid: dkuid | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerc1: dupsacadcareerc1 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerc2: dupsacadcareerc2 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerc3: dupsacadcareerc3 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerdescc1: dupsacadcareerdescc1 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerdescc2: dupsacadcareerdescc2 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadcareerdescc3: dupsacadcareerdescc3 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadprogc1: dupsacadprogc1 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadprogc2: dupsacadprogc2 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsacadprogc3: dupsacadprogc3 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsexpgradtermc1: dupsexpgradtermc1 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsexpgradtermc2: dupsexpgradtermc2 | [?@ != null] | [join(`; ` , @)] | [0],
            dupsexpgradtermc3: dupsexpgradtermc3 | [?@ != null] | [join(`; ` , @)] | [0],
            homedirectory: homedirectory | [?@ != null] | [join(`; ` , @)] | [0]
            }
        """
        return query


class Kuali:
    """
    A utility class that provides the base URL and headers for Kuali API requests.
    """
    @staticmethod
    def url():
        """
        Get the base URL for Kuali API requests.

        Returns:
        str: The base URL for Kuali API requests.
        """
        return "https://duke.kualibuild.com/app/api/v0/graphql"

    @staticmethod
    def headers():
        """
        Get the headers for Kuali API requests.

        The headers include the Authorization token, which is read from an environment variable.

        Returns:
        dict: The headers for Kuali API requests.
        """
        return {
            "Accept-Encoding": "gzip, deflate, br",
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Connection": "keep-alive",
            "DNT": "1",
            "Origin": "https://duke.kualibuild.com",
            "Authorization": f'{os.getenv("AUTH_TOKEN")}',
        }


class KualiQueries:
    """
    A utility class that provides GraphQL queries for retrieving user data from Kuali.
    """
    @staticmethod
    def GetUser():
        """
        Get the GraphQL query for retrieving user data.

        Returns:
        str: The GraphQL query for retrieving user data.
        """
        return """
        query Users($q: String, $skip: Int!, $limit: Int!, $sort: [String!], $active: UserActive) {
        usersConnection(args: {query: $q, skip: $skip, limit: $limit, sort: $sort, active: $active}) {
        edges {
        node {
        id
        displayName
        email
        username
        firstName
        lastName
        active
        approved
        schoolId
        __typename
        }
        }
        }
        }
        """


class KualiMutations:
    """
    A utility class that provides GraphQL mutations for creating users in Kuali.
    """
    @staticmethod
    def CreateUser():
        """
        Get the GraphQL mutation for creating a user.

        Returns:
        str: The GraphQL mutation for creating a user.
        """
        return """
        mutation CreateUser($data: JSON!) {
        createUser(data: $data) {
        ... on User {
        id
        firstName
        lastName
        displayName
        name
        username
        email
        role
        schoolId
        approved
        active
        __typename
        }
        ... on InvalidFieldErrors {
        errors {
        field
        reason
        __typename
        }
        __typename
        }
        __typename
        }
        }
        """


class EnumerableOptions(str, Enum):
    """
    Yet another utility class that provides optional variables for users to query 
    the LDAP service point
    1. cn: Refers to a person's common name.
    2. count: Not sure - but think it may have something to do with a count of a grouped id?
    3. displayname: Refers to a person's display name.
    4. dkuid: Refers to a unique identifier assigned to a person.
    5. dn: Refers to the person's distinguished name in the data management system.
    6. duacmailboxexists: Indicates whether a person has an active mailbox associated with their account.
    7. dudegree: Refers to a person's academic degree(s).
    8. dudempoid: Refers to the Duke Employee ID associated with a person's record.
    9. dudempoidhist: Refers to the Duke Employee ID history associated with a person's record.
    10. dudukeid: Refers to a unique Duke University ID assigned to a person.
    11. dudukeidhistory: Refers to the history of Duke University IDs associated with a person's record.
    12. duldapkey: Refers to the LDAP key associated with a person's record in the data management system.
    13. dumiddlename1: Refers to a person's middle name.
    14. dupsacadcareerc1: Refers to a person's first academic career.
    15. dupsacadcareerc2: Refers to a person's second academic career.
    16. dupsacadcareerc3: Refers to a person's third academic career.
    17. dupsacadcareerdescc1: Refers to the description of a person's first academic career.
    18. dupsacadcareerdescc2: Refers to the description of a person's second academic career.
    19. dupsacadcareerdescc3: Refers to the description of a person's third academic career.
    20. dupsacadprogc1: Refers to a person's first academic program.
    21. dupsacadprogc2: Refers to a person's second academic program.
    22. dupsacadprogc3: Refers to a person's third academic program.
    23. dupsexpgradtermc1: Refers to a person's expected graduation term for their first academic program.
    24. dupsexpgradtermc2: Refers to a person's expected graduation term for their second academic program.
    25. dupsexpgradtermc3: Refers to a person's expected graduation term for their third academic program.
    26. duresearcherid: Refers to a unique researcher ID assigned to a person
    27. dusapcompany: Refers to the company associated with a person's record in the SAP system.
    28. dusapcompanydesc: Refers to the description of the company associated with a person's record in the SAP system.
    29. dusaporgunit: Refers to the organizational unit associated with a person's record in the SAP system.
    30. dusponsor: Refers to the sponsor of a person's record in the data management system.
    31. duuserprivacyrequest: Indicates whether a person has requested privacy settings for their account.
    32. edupersonaffiliation: Refers to a person's affiliation with Duke University (e.g. student, faculty, staff, alumni).
    33. edupersonnickname: Refers to a person's nickname or preferred name.
    34. edupersonprimaryaffiliation: Refers to a person's primary affiliation with Duke University.
    35. edupersonprincipalname: Refers to a person's principal name in the system.
    36. facsimiletelephonenumber: Refers to a person's fax number.
    37. generationqualifier: Refers to a person's generation qualifier (e.g. Jr., Sr.).
    38. gidnumber: Refers to the group ID number associated with a person's uid.
    39. givenname: Refers to a person's first name.
    40. homedirectory: Refers to a person's home directory.
    41. labeleduri: Refers to a labeled URI associated with a person's record.
    42. loginshell: Refers to the person's login shell.
    43. mail: Refers to a person's email address.
    44. ntuserhomedir: Refers to a person's home directory in a Windows-based system.
    45. objectclass: Refers to the object class of a person's record in the data management system.
    46. output: top level item in the json query.
    47. pager: Refers to a pager number associated with a person.
    48. person: Refers to a person's record in the data management system.
    49. sn: Refers to a person's last name or surname.
    50. uid: Refers to a unique user ID assigned to a person in the system.
    51. uidnumber: Refers to the user ID number associated with a person's record.

    returns:
    Same format as seen above, if the value does not exist or is empty, the
    returned value for that key then becomes null.
    """
    cn = "cn"
    count = "count"
    displayname = "displayname"
    dkuid = "dkuid"
    dn = "dn"
    duacmailboxexists = "duacmailboxexists"
    dudegree = "dudegree"
    dudempoid = "dudempoid"
    dudempoidhist = "dudempoidhist"
    dudukeid = "dudukeid"
    dudukeidhistory = "dudukeidhistory"
    duldapkey = "duldapkey"
    dumiddlename1 = "dumiddlename1"
    dupsacadcareerc1 = "dupsacadcareerc1"
    dupsacadcareerc2 = "dupsacadcareerc2"
    dupsacadcareerc3 = "dupsacadcareerc3"
    dupsacadcareerdescc1 = "dupsacadcareerdescc1"
    dupsacadcareerdescc2 = "dupsacadcareerdescc2"
    dupsacadcareerdescc3 = "dupsacadcareerdescc3"
    dupsacadprogc1 = "dupsacadprogc1"
    dupsacadprogc2 = "dupsacadprogc2"
    dupsacadprogc3 = "dupsacadprogc3"
    dupsexpgradtermc1 = "dupsexpgradtermc1"
    dupsexpgradtermc2 = "dupsexpgradtermc2"
    dupsexpgradtermc3 = "dupsexpgradtermc3"
    duresearcherid = "duresearcherid"
    dusapcompany = "dusapcompany"
    dusapcompanydesc = "dusapcompanydesc"
    dusaporgunit = "dusaporgunit"
    dusponsor = "dusponsor"
    duuserprivacyrequest = "duuserprivacyrequest"
    edupersonaffiliation = "edupersonaffiliation"
    edupersonnickname = "edupersonnickname"
    edupersonprimaryaffiliation = "edupersonprimaryaffiliation"
    edupersonprincipalname = "edupersonprincipalname"
    facsimiletelephonenumber = "facsimiletelephonenumber"
    generationqualifier = "generationqualifier"
    gidnumber = "gidnumber"
    givenname = "givenname"
    homedirectory = "homedirectory"
    labeleduri = "labeleduri"
    loginshell = "loginshell"
    mail = "mail"
    ntuserhomedir = "ntuserhomedir"
    objectclass = "objectclass"
    ou = "ou"
    output = "output"
    pager = "pager"
    person = "person"
    sn = "sn"
    title = "title"
    uid = "uid"
    uidnumber = "uidnumber"


class SearchLDAPUsers(BaseModel):
    """
    A Pydantic model that represents the required fields for the "search_user" FastAPI route.

    Attributes:
    requester (str): The display name from Kuali Build of the person making the request.
    search (str): The search term to use.
    option (EnumerableOptions): The option to use for the search.
    """
    search: str
    option: EnumerableOptions

class KualiUser(BaseModel):
    """
    A Pydantic model that represents the required fields for the "add_user_to_kuali_build" FastAPI route under 'verify_kuali_user_ldap' key.

    Attributes:
    requester (str): The display name from Kuali of the person making the request.
    dukeid (str): The Duke ID of the user to search and/or add.
    firstName (str): The first name of the user to search and/or add.
    lastName (str): The last name of the user to search and/or add.
    netid (str): The NetID of the user to search and/or add.
    email (str): The email address of the user to search and/or add.
    """
    dukeid: str
    firstName: str
    lastName: str
    netid: str
    email: str


class SearchOrAddKualiUsers(BaseModel):
    displayName: str
    verify_kuali_user_ldap: KualiUser

# Kuali User Manager

[![pipeline status](https://gitlab.oit.duke.edu/sjc98/kb-search-add-external-users/badges/master/pipeline.svg)](https://gitlab.oit.duke.edu/sjc98/kb-search-add-external-users/-/commits/master)

This code uses FastAPI to expose an HTTP API for getting or creating a user from the Kuali Build User Database and manages interactions with the Kuali Build User Database. It primarily enables the searching and addition of users within the Kuali user database.

## Global Setup

The `HTTPBasic` security scheme is instantiated and the `.env` file is loaded using `dotenv`.

## Function: `verify_credentials(credentials: HTTPBasicCredentials = Depends(security))`

This function checks if the provided credentials are valid by comparing them with the correct credentials stored in environment variables. It raises an HTTPException with a status code of 401 if the credentials are invalid.

## FastAPI Application: `app`

An instance of `FastAPI` is created as `app`. 

### Exception Handler: `http_exception_handler(request: Request, exc: HTTPException)`

This exception handler catches `HTTPException` errors and returns them in a standardized format using `ORJSONResponse`.

### Endpoint: `GET /api/v1/get/get-or-create/kuali/user`

This endpoint accepts a GET request and depends on the `verify_credentials` function for Basic Authentication. If the credentials are valid, it uses the `KualiUserManager` from the previous code snippet to either get an existing user or create a new one in the Kuali database.

### Main Execution

If this script is run as the main script, it will start a Uvicorn server, running the FastAPI application on the host and port specified. 

## Dependencies

The script uses several Python libraries, including:

- `os`
- `asyncio`
- `json`
- `httpx`
- `loguru`
- `fastapi`

In addition, it uses some local modules:

- `kuali_users`
- `data_structures`

## Class: `KualiUserManager`

This class manages all interactions with Kuali.

### Method: `fetch_kuali_data(query: str, variables: dict)`

This method makes HTTP post requests to a Kuali server, sending a GraphQL query with some variables. It implements retry logic using exponential backoff for the case where the request fails. The HTTP request is made asynchronously with the httpx async client.

### Method: `get_user_from_kuali(kuali_query, kuali_variables)`

This method fetches user data from Kuali by using the `fetch_kuali_data` method. It handles errors when the expected user data is not found in the response.

### Method: `create_user_in_kuali(dukeid: str, firstName: str, lastName: str, netid: str, email: str)`

This method creates a new user in the Kuali system. The user's information is defined in the `user_data` dictionary. The method uses the `fetch_kuali_data` method to send a GraphQL mutation to the Kuali server, and logs if the user was created successfully.

### Method: `get_or_create_user_from_kuali(dukeid: str, firstName: str, lastName: str, netid: str, email: str)`

This method combines the `get_user_from_kuali` and `create_user_in_kuali` methods. First, it tries to get the user information from Kuali. If the user does not exist, it creates a new user. If there is an error during user creation, it logs an error message and re-raises the exception. The function returns the user's information, whether the user was found or newly created.

### Method: `extract_user_information(user_node, provisioning_status)`

This method formats the user data retrieved from Kuali into a Python dictionary. The user's provisioning status is also included in the dictionary.

## Logging

The script uses the `loguru` library to handle logging. Log messages are written to the file specified by the `logger.add()` method.


## Running the Application
To start the server, run the script directly or use Uvicorn:

```bash
python endpoints.py
```
### or

```bash
uvicorn endpoints:app --host=0.0.0.0 --port=8099 --proxy-headers --reload --root-path="/kbsearchaddexternalusers" --loop="uvloop" --http="httptools"
```

## Project Architecture

This project contains instructions for 2 containers - 

1. web-kb-search-add-external-users
2. caddy-kb-search-add-external-users

## Project Structure

The main project file is `endpoints.py` which contains our general framework/setup, middleware, routes and requested business logic. It uses the FastAPI framework to create the APIs, and Pydantic library for data validation.

## Installation<br>

Before you can run this project, you'll need to install the dependencies.<br>
Docker engine:
```sh
sudo apt-get update \
    && sudo apt-get install docker.io docker-doc docker-compose podman-docker containerd runc
```
## Docker Compose<br>

** Please note:  kb-search-add-external-users is the project name as named for this project directory in GitLab<br>

1. The `web` service:
    - The Docker image is built from the current directory, tagged as `kuali-kb-search-add-external-users`.<br>
    - The container is named `web-kb-search-add-external-users`.<br>
    - The container exposes port 8099 to ahe host at port 0.0.0.0:8099.<br>
    - Two volumes are mounted to the container. The first volume mounts the `.env` configuration file from the host to the `/app/.env` path in the container. The second volume mounts the log directory from the host to the `/app/log/` path in the container.<br>
    - The container will be restarted automatically unless it is explicitly stopped.<br>
    - The container is connected to the `caddy_network` network.<br>

2. The `caddy` service:
    - The image used is the latest Caddy server image.<br>
    - The container is named `caddy-kb-search-add-external-users`.<br>
    - The container is set to restart unless stopped (for maintenance and trouble-shooting).<br>
    - Three volumes are mounted to the container. The first volume mounts the `Caddyfile` from the host to `/etc/caddy/Caddyfile` in the container. The second volume mounts the `/etc/caddy` directory from the host to `/etc/caddy` in the container. The third volume mounts the Docker socket, allowing Caddy to interact with Docker.<br>
    - The container exposes ports 80, 443, and 443/udp to the host at ports 7095, 8449, and 8449 respectively.<br>
    - The container is connected to the `caddy_network` network.<br>

3. The `caddy_network` network:
    - On this dev machine, I had created a docker network named `caddy_network` with the bridge driver.<br>

The configuration allows our two services to communicate with each other over the caddy network. In short, our web service runs the FastAPI application, while the caddy service runs a Caddy server that acts as a reverse proxy for this project.  The other benefit of Caddy is the use of automatically obtained/renewed SSL/TLS certificates from Let's Encrypt, and also handles OCSP stapling.  Caddy also uses modern protocols and technologies like HTTP/2 and QUIC to improve both speeds + reliability of our applications (speed increase of nearly 6x HTTP/1.1 in my testing).

You can learn more about caddy and how awesome it is at https://caddyserver.com/docs/<br>

- Please note that you will need to create your own `/etc/caddy/Caddyfile`, `/etc/caddy`, and `/srv/persistent-data/app/kb-search-add-external-users/.env` files and directories on your host as they are required by the Docker configuration.

## Environment variables<br>

```ruby
- AUTH_TOKEN='xxx_kuali_build_api_token_xxx'
- BASIC_AUTH_USERNAME='xxx_create_username_xxx'
- BASIC_AUTH_PASSWORD='xxx_create_password_xxx'
```

## Installation<br>

1. Clone the repository to your server or linux/unix-based system.
2. Navigate to the project directory in a terminal.
3. Build the Docker image using the following command:

```bash
docker-compose build
```

## Usage

- To test the application in a Docker container, use the following command:

```bash
docker-compose up
```

- To shut down this test, use the following command:

```bash
docker-compose down
```

- To run the application in the background, use the following command:

```bash
docker-compose up -d
```
# Third-party imports
import httpx
from jmespath import search as jsearch

# Local application imports
from data_structures import JmespathQueries


class LDAPDataFetcher:
    def __init__(self, option_value, search, email=None):
        self.timeout = 60.0
        self.http2 = True
        self.option_value = option_value
        self.search = f"*{search}*"
        self.email = email

    async def fetch(self, url, client):
        return await client.get(url)

    def replace_dollar_sign(self, query):
        for person in query:
            for key, value in person.items():
                if isinstance(value, str):
                    person[key] = value.replace('$', ', ')

    async def get_data_from_pof(self):
        url = f"https://pof.trinity.duke.edu/feed/ldap/default/{self.option_value}/{self.search}"

        async with httpx.AsyncClient(timeout=self.timeout, http2=self.http2) as client:
            response = await self.fetch(url, client)

        if response.status_code == 200:
            query = jsearch(JmespathQueries.duke_members(), response.json())
            self.replace_dollar_sign(query)
            all_users = []
            for user_data in query:
                displayname = user_data['displayname']
                title = user_data['title'] or user_data['edupersonprimaryaffiliation']
                department = user_data['ou'] or "Dept Not listed"
                mail = user_data['mail'] or "Email Not listed"
                dudukeid = user_data['dudukeid'] or None
                givenname = user_data['givenname'] or None
                sn = user_data['sn'] or None
                uid = user_data['uid'] or None
                user = {
                    "displayname": f"{displayname} ⁘ {title} ⁘ {department} ⁘ {mail}",
                    "verify_kuali_user_ldap": {
                        "dukeid": dudukeid,
                        "firstName": givenname,
                        "lastName": sn,
                        "netid": uid,
                        "email": mail
                    }
                }
                all_users.append(user)
            return all_users
        else:
            raise HTTPException(status_code=400, detail="Something went wrong")


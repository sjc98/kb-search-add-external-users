# Standard library imports
import os
import asyncio
import json

# Third-party imports
import httpx
from loguru import logger
from fastapi import HTTPException

# Local application imports
from data_structures import KualiQueries, KualiMutations, Kuali


class KualiUserManager:
    @staticmethod
    async def fetch_kuali_data(query: str, variables: dict):
        url = Kuali.url()
        headers = Kuali.headers()

        retries = 3
        backoff_factor = 0.1

        for retry in range(retries):
            try:
                async with httpx.AsyncClient(http2=True, timeout=10.0) as client:
                    response = await client.post(url, json={"query": query, "variables": variables}, headers=headers)
                    print(response)

                    if response.status_code != 200:
                        raise HTTPException(
                            status_code=400, detail="Something went wrong with the Kuali query")

                    return response.json()

            except httpx.RequestError as oops:
                if retry == retries - 1:
                    logger.error(
                        f"Request failed after {retries} retries. Error: {oops}")
                    raise HTTPException(
                        status_code=500, detail="Maximum retries exceeded with the Kuali query")

                else:
                    logger.warning(
                        f"Request failed. Retrying in {backoff_factor * (2 ** retry)}s. Error: {oops}")
                    await asyncio.sleep(backoff_factor * (2 ** retry))

    @classmethod
    async def get_user_from_kuali(cls, kuali_query, kuali_variables):
        kuali_response = await cls.fetch_kuali_data(kuali_query, kuali_variables)
        try:
            user_node = kuali_response["data"]["usersConnection"]["edges"][0]["node"]
        except (IndexError, KeyError):
            user_node = None
        return user_node

    @classmethod
    async def create_user_in_kuali(cls, dukeid: str, firstName: str, lastName: str, netid: str, email: str):
        create_kuali_user_mutation = KualiMutations.CreateUser()
        user_data = {
            "role": "user",
            "approved": True,
            "firstName": firstName,
            "lastName": lastName,
            "name": f"{firstName} {lastName}",
            "username": f"{netid}@duke.edu",
            "email": email or f"{netid}@duke.edu",
            "schoolId": dukeid,
        }
        create_user_variables = {"data": user_data}
        created_user = await cls.fetch_kuali_data(create_kuali_user_mutation, create_user_variables)
        logger.info(f'Kuali User Created: {created_user}')
        return created_user

    @classmethod
    async def get_or_create_user_from_kuali(cls, dukeid: str, firstName: str, lastName: str, netid: str, email: str):
        kuali_query = KualiQueries.GetUser()
        kuali_variables = {
            "active": "ACTIVE",
            "limit": 1,
            "skip": 0,
            "sort": [],
            "q": dukeid,
        }

        user_node = await cls.get_user_from_kuali(kuali_query, kuali_variables)

        if user_node is not None:
            results = cls.extract_user_information(user_node, f'☑️ {firstName} {lastName}')
        else:
            try:
                created_user = await cls.create_user_in_kuali(dukeid, firstName, lastName, netid, email)
                user_node_after_creation = await cls.get_user_from_kuali(kuali_query, kuali_variables)

                if not user_node_after_creation:
                    logger.error(
                        "Failed to fetch the newly created user from Kuali")
                    raise ValueError(
                        "Failed to fetch the newly created user from Kuali")

                results = cls.extract_user_information(user_node_after_creation, f'✅ {firstName} {lastName}')

            except Exception as whoops:
                logger.error(f"Error occurred during user creation: {whoops}")
                raise

        return results


    @staticmethod
    def extract_user_information(user_node, result): 
        return {
            "status": result,
            "typeahead": {
                "id": user_node["id"],
                "label": user_node["username"]
            },
            "kuali_user_status": {
                "dukeid": user_node["schoolId"],
                "firstName": user_node["firstName"] or 'Unknown',
                "lastName": user_node["lastName"] or 'Unknown',
                "netid": user_node["username"],
                "email": user_node["email"] or user_node["username"],
            }
        }

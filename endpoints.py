# Standard Libs
import sys
import os
import pwd
import json
import time
import socket
from datetime import datetime, timedelta
import pytz
from typing import Optional, List
import asyncio
from asyncio import gather, all_tasks, current_task
from tempfile import gettempdir
import fcntl

# External Libs
import aiofiles
import httptools
import uvloop
import uvicorn
from pydantic import BaseModel
from fastapi import FastAPI, Depends, HTTPException, Request, Query
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse, ORJSONResponse
from starlette.requests import Headers
from loguru import logger
from dotenv import load_dotenv

# Local application imports
from data_structures import SearchLDAPUsers
from kuali_users import KualiUserManager
from ldap_feed import LDAPDataFetcher

HOST = "0.0.0.0"
PORT = 8099
ROOT_PATH = "/kbsearchaddexternalusers"

# Set up basic authentication
security = HTTPBasic()
uvloop.install()
load_dotenv()

# Ensure we are relating imports to are current directory
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(current_dir)

# Set up logging
log_file = os.path.join(current_dir, 'log', 'kb-search-add-external-users.log')
logs = logger.add(
    log_file,
    format="{time:YYYY-MM-DD at HH:mm:ss} {exception} | {level} | {message} | {line} | {module}",
    level="INFO",
    enqueue=True
)


# Initialize FastAPI app
app = FastAPI(
    openapi_url=None,
    docs_url=None,
    redoc_url=None,
    default_response_class=ORJSONResponse
)


def verify_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = os.getenv('BASIC_AUTH_USERNAME')
    correct_password = os.getenv('BASIC_AUTH_PASSWORD')
    if credentials.username != correct_username or credentials.password != correct_password:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password"
        )
    return credentials


def process_headers(headers: Headers) -> dict:
    required_headers = ["referer", "x-kuali-origin", "x-kuali-user-id", "x-kuali-user-ssoid"]
    return {header: headers.get(header) for header in required_headers}


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    headers = process_headers(request.headers)
    referer = headers.get('referer') or f"{headers['x-kuali-origin']} | {headers['x-kuali-user-id']} | {headers['x-kuali-user-ssoid']}"
    query_string = request.query_params._list

    start_time = time.time()
    try:
        response = await call_next(request)
        process_time_str = str(timedelta(seconds=time.time() - start_time))
        response.headers["X-Process-Time"] = process_time_str

        log_msg = f"{referer} | {query_string} | {process_time_str} | {request.url.path} | {response.status_code}"
        logger.success(log_msg)
        return response
    except Exception as dammit:
        error_msg = f"{referer} | {query_string} | |{request.url.path} | | {str(dammit)}"
        logger.error(error_msg)
        raise dammit


async def cleanup_logs(days=14):
    now = datetime.now(pytz.timezone('US/Eastern'))
    formatted_time = now.strftime("%Y-%m-%d")
    cutoff = now - timedelta(days=days)
    cutoff = cutoff.replace(tzinfo=None)

    async with aiofiles.open(log_file, mode='r') as f:
        lines = await f.readlines()

    new_lines = []
    for line in lines:
        elements = line.split(" at ", 1)
        log_time_str = " ".join(elements[:1])
        try:
            log_time = datetime.strptime(log_time_str, "%Y-%m-%d")
        except ValueError:
            continue

        if log_time > cutoff:
            new_lines.append(line)

    async with aiofiles.open(log_file, mode='w') as f:
        await f.write(''.join(new_lines))


# Create a lock file so workers do not duplicate one another
LOCKFILE = os.path.join(gettempdir(), 'Workers.lock')


@app.on_event("startup")
async def startup_event():
    with open(LOCKFILE, 'w') as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            return  # Another instance has the lock, we should exit.

        # If we reach here, we have the lock and can safely execute the startup tasks.
        await cleanup_logs()
        logger.info("Logs scrubbed")
    uid = os.getuid()
    username = pwd.getpwuid(uid).pw_name
    hostname = socket.gethostname()
    logger.info(f"Initializing on host {hostname}")
    logger.info(f"Parent directory is {parent_dir}")
    logger.info(f"Current directory is {current_dir}")
    logger.info(f"User is {username}")
    logger.success(f"Application Started on {HOST}:{PORT}")
    logger.info(f"Route: {ROOT_PATH}/*")

@app.on_event("shutdown")
async def shutdown_event():
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    [task.cancel() for task in tasks]
    try:
        await asyncio.gather(*tasks, return_exceptions=False)
    except asyncio.exceptions.CancelledError:
        logger.opt(exception=True).debug("A task was cancelled")
    logger.success(f"Cancelled {len(tasks)} outstanding tasks")

class UserIn(BaseModel):
    dukeid: str
    firstName: str
    lastName: str
    netid: str
    email: str

class KualiUserStatus(BaseModel):
    dukeid: str
    firstName: str
    lastName: str
    netid: str
    email: str

class Typeahead(BaseModel):
    id: Optional[str]
    label: Optional[str]

class UserOut(BaseModel):
    status: Optional[str]
    typeahead: Optional[Typeahead]
    kuali_user_status: KualiUserStatus

def query_parser(query_param: str):
    parsed_query = jsonable_encoder(query_param)
    return parsed_query

# @app.exception_handler(HTTPException)
@app.post("/api/v1/get/get-or-create/kuali/user", response_model=UserOut, dependencies=[Depends(verify_credentials)],
         tags=["Kuali search for and/or add users to the Kuali Build user database"])
async def add_user_to_kuali_build(verify_kuali_user_ldap: str = Query(...)):
    user_data = json.loads(verify_kuali_user_ldap)
    dukeid = user_data['dukeid']
    firstName = user_data['firstName']
    lastName = user_data['lastName']
    netid = user_data['netid']
    email = user_data['email']
    result = await KualiUserManager.get_or_create_user_from_kuali(dukeid, firstName, lastName, netid, email)
    return result


# Define a GET route to search for LDAP users
@app.get("/api/v1/get/ldap/all/users", dependencies=[Depends(verify_credentials)], tags=["Trinity"])
async def search_user(search_user_params: SearchLDAPUsers = Depends()):
    ldap_data = LDAPDataFetcher(search_user_params.option.value, search_user_params.search)
    result = await ldap_data.get_data_from_pof()
    return result

# remember to remove port and route - note to self

if __name__ == "__main__":
    uvicorn.run(
        __name__ + ":app",
        host=HOST,
        port=PORT,
        proxy_headers=True,
        reload=False,
        root_path=ROOT_PATH,
        loop="uvloop",
        http="httptools",
        workers=4,
        log_level="info",
        timeout_keep_alive=int(os.environ.get("KEEP_ALIVE", 60)),
        limit_max_requests=int(os.environ.get("MAX_REQUESTS", 1000)),
    )